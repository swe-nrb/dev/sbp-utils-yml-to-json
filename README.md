# DEPRECATED

Please use https://gitlab.com/infopack/infopack-gen-file-parser instead.

-------------------------------------------------------


This package reads Yaml file(s) from a directory and returns a json object

# Maintainers

* [Johan Asplund](https://www.linkedin.com/in/bimjohan/)

## Usage

```
var ymlToJSON = require('sbp-utils-yml-to-json');

var pipeline = new Pipeline();

pipeline.addStep(ymlToJSON.step());
```

### Custom settings

The step has the following parameters

| Parameter    | Required  | Description                                                  | Default                                      |
|--------------|-----------|--------------------------------------------------------------|----------------------------------------------|
| readPath     | No        | Path to the folder containing the yml-files                  | `./raw_files`                                |
| setKey       | No        | If true the result will be set to the pipeline key-value set | `true`                                       |
| writeFile    | No        | If true the result will be written to a file           | `true`                                             |
| writePath    | No        | Path to the output file folder                         | 1: `pipeline.getWorkDirPath()` 2: `./work_folder`) |
| fileName     | No        | Name of the output file (without file ending)          | `collection`                                       |

**Example**

```
pipeline.addStep(ymlToJSON.step({
	writePath: './some_custom_folder',
	fileName: 'some_name'
}));
```